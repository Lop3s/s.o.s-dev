import styled from "styled-components";

const StyledContainer = styled.div`
  display: flex;
  align-items: center;
  height: 50vh;
  width: 100vw;
  max-width: 100%;

  .sloganbutton {
    width: 50vw;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;

    img {
      margin-bottom: 6vh;
      width: 35vw;
      height: auto;

      @media (min-width: 1440px){
        width: 30vw;
      }
    }
  }

  .pessoas {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 50vw;

      img {
        width: 35vw;
        height: auto;
        @media (min-width: 1440px){
        width: 30vw;
      }
      }
  }
`;
export default StyledContainer;
