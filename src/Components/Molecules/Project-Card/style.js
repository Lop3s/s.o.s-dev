import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    width: "50vw",
    margin: "2vh auto",
    flexGrow: 1,
    wordBreak: "break-all",
    overflowY: "auto",
    "@media(minWidth: 720px)": {
      ".makeStyles-root-36": {
        width: "90%",
      },
    },
    "@media (min-width: 280px)": {
      width: '95%',
      margin: '1vh 0px'
    }
  },

  cardHeader: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
  },
  content: {
    margin: theme.spacing(1),

    display: "flex",
    textAlign: "center",
    alignItems: "center",
    height: "30vh",
  },

  contentItem: {
    height: "30vh",
    maxHeight: "24vh",
    backgroundColor: "#17223a",
    padding: "10px",
  },
  contentItemStacksTech: {
    height: "30vh",
    maxHeight: "24vh",
    backgroundColor: "#17223a",
    display: "flex",
    flexDirection: "column",
  },
  statsItem: {
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    flexDirection: "column",
  },
  statsIcon: {
    margin: theme.spacing(1),
  },

  fontsize: {
    fontSize: '2vw important',
  }
}));

export default useStyles;
