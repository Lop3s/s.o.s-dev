import { makeStyles } from "@material-ui/core/styles";

const Style = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
    marginBottom: "0px",
  },

  titlePopUp: {
    color: "blue",
  },
  textCard: {
    width: "90%",
  },

  commentTitle: {
    marginRight: "1vw",
    "@media (min-width: 768px)": {
      fontSize: '2.3vw',
    },
    "@media (min-width: 1440px)": {
      fontSize: '2vw',
    },
  },

  commentContent: {
    "@media (min-width: 768px)": {
      fontSize: '2vw',
    },
    "@media (min-width: 1440px)": {
      fontSize: '1vw',
    },
  },

  fontStyleProfile: {
    color: "#EFDCBD",
    fontSize: '1.7vw',
    "@media(min-width: 280px) and (max-width: 320px)": {
      fontSize: '4vw',
    },
    "@media(min-width: 320px) and (max-width: 540px)": {
      fontSize: '3vw',
    },    
    "@media(min-width: 540px) and (max-width 768px)": {
      fontSize: '2.5vw',
    },
    "@media(min-width: 1440px)": {
      fontSize: '1vw',
    },
  },

  profileContacts: {
    color: "#EFDCBD",
  },

  fontStyleProfileName: {
    color: "#EFDCBD",
    fontSize: '2.5vw',
    "@media(min-width: 280px) and (max-width: 320px)": {
      fontSize: '5vw',
    },
    "@media(min-width: 320px) and (max-width: 540px)": {
      fontSize: '4.5vw',
    },
    "@media(min-width: 540px) and (max-width 768px)": {
      fontSize: '4vw',
    },
    "@media(min-width: 1440px)": {
      fontSize: '1.8vw',
    },
    "@media(min-width: 2560px)": {
      fontSize: '1.5vw',
    }
  },

  commentDescription: {
    fontSize: '3vw',
  },

  descriptionContent: {
    "@media (min-width: 768px)": {
      fontSize: '2vw',
    },
    "@media(min-width: 1024px)": {
      fontSize: '1.8vw',
    },
    "@media(min-width: 1440px)": {
      fontSize: '1.6vw',
    },
    "@media(min-width: 2560px)": {
      fontSize: '1.5vw',
    }
  },

  titleCard: {
    "@media(min-width: 2560px)": {
      fontSize: '2vw',
    }
  },
  
  typeCard: {
    "@media(min-width: 2560px)": {
      fontSize: '1.8vw',
    }
  },

  descriptionCard: {
    "@media(min-width: 2560px)": {
      fontSize: '1.5vw',
    }
  }
  
}));

export default Style;
