import { makeStyles } from "@material-ui/core/styles";

const Style = makeStyles((theme) => ({
  inputSearch: {
    width: "18vw",
  },
  inputSelect: {
    width: "inherit",
    fontSize: "0.5rem",
  },
}));

export default Style;
