import styled from "styled-components";
import Popup from "reactjs-popup";

export const StyledPopup = styled(Popup)`
  &-overlay {
    height: 100vh;
    width: 100vw;
    background: rgba(10, 11, 11, 0.5);
  }

  &-content {
    height: 65%;
    width: 35%;
    background: #17223a;
    border: 5px solid #efdcbd;
    border-radius: 20px;
  }
`;

export const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const StyledButton = styled.button`
  width: 10%;
  background: none;
  outline: none;
  border: none;
  cursor: pointer;
  margin: 5px;
  img {
    width: 32px;
    height: 32px;
  }
`;

export const StyledButtonSave = styled.button`
  height: 100%;
  width: 100%;
  background: #e63462;
  outline: none;
  cursor: pointer;
  border: none;
  border-radius: 5px;
  color: white;
  font-weight: bold;
  font-size: 1.3em;
  :hover {
    background: #e84670;
  }
`;

export const ButtonContent = styled.div`
  height: 10%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const ImgContent = styled.div`
  height: 90%;
  width: 100%;
  display: flex;

  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
`;
export const Image = styled.img`
  width: 70px;
  height: 70px;
  margin: 5px;
  cursor: pointer;

  :hover {
    transform: scale(1.1);
  }

  :active {
    filter: invert(20%);
  }
  :focus {
    transform: scale(1.1);
    filter: invert(20%);
  }
`;
