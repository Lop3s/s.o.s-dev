import { useState, useEffect } from "react";
import { api } from "../../../axios-globalConfig/axios-global";
import {
  StyledPopup,
  Container,
  StyledButton,
  Image,
  ImgContent,
  ButtonContent,
  StyledButtonSave,
} from "./style";

import { useSelector, useDispatch } from "react-redux";

import { getProfileThunk } from "../../../Redux/modules/profile/thunks";

const PopupImgChange = () => {
  const [allIcons, setAllIcons] = useState();
  const [selectIcon, setSelectIcon] = useState("");
  const { profile } = useSelector((state) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    api.get("/icons").then((res) => {
      setAllIcons(res.data);
    });
  }, [setAllIcons]);

  const handleIcon = (e) => {
    api
      .patch(
        `/users/${profile.id}`,
        { src: selectIcon },
        {
          headers: {
            Authorization: `Bearer ${profile.token}`,
          },
        }
      )
      .then((res) => {
        dispatch(getProfileThunk(profile.email, profile.token));
      });
  };

  return (
    <>
      <StyledPopup
        trigger={
          <StyledButton>
            <img
              src="https://img.icons8.com/dusk/64/000000/change-user-male.png"
              alt="profile"
            />
          </StyledButton>
        }
        modal
        nested
      >
        {(close) => (
          <Container>
            <ImgContent>
              {allIcons?.map((e) => (
                <Image
                  src={e.link}
                  onClick={() => {
                    setSelectIcon(e.link);
                    console.log(selectIcon);
                  }}
                />
              ))}
            </ImgContent>
            <ButtonContent>
              <StyledButtonSave onClick={handleIcon}>SALVAR</StyledButtonSave>
            </ButtonContent>
          </Container>
        )}
      </StyledPopup>
    </>
  );
};

export default PopupImgChange;
