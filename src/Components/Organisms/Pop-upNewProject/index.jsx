import { useState } from "react";
import Button from "../../Atoms/Button";

import { StyledPopup, Container } from "./style";

import Projects from "../../Molecules/Projects";

export const PopupNewProject = ({ setAuth }) => {
  return (
    <>
      <StyledPopup
        trigger={<Button text="Novo Projeto" classe="profileFavorites" />}
        modal
        nested
      >
        {(close) => (
          <Container>
            <Projects text="Criar Projeto" close={close} setAuth={setAuth} />
          </Container>
        )}
      </StyledPopup>
    </>
  );
};

export default PopupNewProject;
