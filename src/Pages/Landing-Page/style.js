import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 200vh;
  background-color: #0C101D;
  margin-top: -2vh;
`;

export const BoxComments = styled.div`
  width: 100%;
  height: 30%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
