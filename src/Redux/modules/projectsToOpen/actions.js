

const addOpenProject = (openProject) => ({
    type: "@openProject",
    openProject
});

export default addOpenProject;