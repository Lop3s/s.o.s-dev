const addFilteredProjects = (filteredProjects) => ({
    type: "@add/filteredProjects",
    filteredProjects
});

export default addFilteredProjects;